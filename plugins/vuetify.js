import Nuxt from "nuxt";
import Vuetify from "vuetify/lib";

import colors from "vuetify/lib/util/colors";

Nuxt.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      dark: {
        primary: colors.lightBlue.lighten4, // #B3E5FC
        secondary: "#fcdeb3",
        accent: "#fcb9b3",
        error: "#ffffff"
      }
    }
  }
});
